#!/usr/bin/env bash

# page to display; note the (not well documented) parameters where Grafana is concerned
URL="https://grafana.example.com/wherever-your/dashboard-is?refresh=1m&kiosk"

# disable display blanking
xset s noblank
xset s off
xset -dpms

# show computer's IP address at start
(
  yad=$(command -v yad)
  if [[ -x "$yad" ]]; then
    "$yad" --timeout=30 --timeout-indicator=bottom --button=gtk-ok:0 --center --borders=20 --on-top \
      --title="my IP address(es)" --text="<span font=\"100\">$(hostname -I | tr ' ' \\n)</span>" || true
  fi
) &

# sync clock, not necessary outside RPi
#sudo --non-interactive ntpdate ntp.nic.cz || true

# move mouse top left
xdotool mousemove 0 0 || true
# hide mouse
(unclutter -idle 0.5 -root || true) &

# clean cache
rm -rf ~/.cache/chromium/Default/Cache/Cache_Data/* || true
# do not prompt to restore previous pages
if [[ -x "$(command -v sed)" ]]; then
  sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/Default/Preferences
  sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences
fi

# show kiosk mode
exec /usr/bin/chromium-browser --no-crash-upload --noerrdialogs --disable-infobars --disable-component-update --kiosk "${URL}"
