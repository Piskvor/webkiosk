# webkiosk

Kiosk mode to load in Chromium (made for a single-page RPi dashboard)

## TL;DR:
- have a box with X11 and chromium-browser
- clone this
- log into dashboard in browser, manually
- start service

### Requires:
- a box with X11, and probably autologin (e.g. a RPi with Raspbian, but the script is fairly generic, linux-ish)
- `chromium-browser`

### Suggested:
- `xdotool` (automate keyboard+mouse)
- `sed` (stream editor)
- `unclutter` (hide mouse)
- `ntpdate` (sync clock once)
- `git` (clone from repo)
- `yad` (dialogs from command line, shows IP address)
- `crontab` (restart the service e.g. daily; replaceable with systemd kiosk-restart.service and kiosk-restart.timer)

Runs from `systemd` as kiosk.service (which is just a convenient way to run `env DISPLAY=:0.0 XAUTHORITY=/home/pi/.Xauthority /home/pi/webkiosk/kiosk.sh`, see the kiosk.service file)

## Installation

- `sudo apt install -y xdotool unclutter sed chromium-browser unclutter ntpdate git yad`
- start chromium-browser manually, login to the dashboard, copy URL, close browser (use vncssh if not physically at computer)
- `git clone ... ~/webkiosk`
- edit `kiosk.sh`, paste the URL from dashboard to `URL=""`
- RPi-specific tweaks:
  - `sudo raspi-config`, System, and S1 Set Hostname (so you don't have 20 hosts on a network, all claiming to be named `raspberrypi` - in our case, we used `xdashboard`)
  - optionally set autologin to GUI

- `sudo systemctl edit --full kiosk`
- paste kiosk.service; note that the `ExecStart` path matches wherever you cloned the repo, alter user and group as needed
- `sudo systemctl enable --now kiosk`
- optionally `crontab -e` and set the restart line
  - or `sudo systemctl edit --full kiosk-restart.service`, `sudo systemctl edit --full kiosk-restart.timer` and `sudo systemctl enable --now kiosk-restart.timer`

## Periodic time sync - setup (necessary on RPi - clock without NTP will go crazy, HTTPS therefore too)
- edit `/etc/ntp.conf` and add e.g. `server ntp.nic.cz`
- edit `/etc/systemd/timesyncd.conf` and add `NTP=ntp.nic.cz` (same hostname as above; apparently both are needed?)
- sync manually for the first time: `sudo ntpdate ntp.nic.cz` (in normal operation, NTP will not update if the difference is too great, e.g. the local system clock is in 2020)
- `sudo timedatectl set-ntp true`
- `sudo systemctl restart systemd-timesyncd`

## Tab crash workaround (optional)

Some dashboards are MASSIVE and tend to crash when out of RAM. For this, launch Chromium manually and install the following extension:
- [Reload Crashed Tabs](https://chrome.google.com/webstore/detail/reload-crashed-tabs/blphpdkmdeipohhgpajljkfgmhhllhem)
- no extension configuration needed

## Discoverability (optional)

If you need to access the device from outside, instead of fumbling with keyboards and meeces:

### Required:
- `ssh` (OpenSSH server, is probably installed by default)

### Suggested:
- `yad` (shows IP address on service re/start), and/or
- `avahi-daemon` (make the device discoverable at [xdashboard].local), and/or
- `x11vnc` (access the kiosk's screen remotely)

Script for accessing the kiosk's screen from a local Linux/MacOSX system:
- `vncssh` (in this repo, requires `ssvnc` locally and `x11vnc` on kiosk)
- (any other VNC viewer through a SSH port forward could work, e.g. putty+vncviewer, this just automates the steps)
- usage: `vncssh pi@xdashboard.local`

## Reloading the page manually (optional)

- this is a normal web page reload, for lesser issues that don't need reloading the service - we're literally sending an `F5` to the foreground window
- `ssh pi@xdashboard.local -- env DISPLAY=:0 xdotool key F5`

## Updating this repo on kiosk (optional)

- have your SSH key registered at GitLab (or wherever your repo is)
- forward the SSH agent, e.g.:
- `ssh pi@xdashboard.local -A`
- `cd webkiosk` (or wherever your checkout dir is)
- `git stash && git pull && git stash pop` (preserve local modifications and merge afterwards), or
- !!! `git fetch && git reset origin/master --hard` (update to remote version, DISCARD any local modifications)
